# Utiliser des pods statiques

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Introduction à l'utilisation des pods statiques

Bonjour et bienvenue dans cette leçon où nous allons parler de l'utilisation des pods statiques. Voici un aperçu rapide des sujets que nous allons aborder :

1. Qu'est-ce qu'un pod statique ?
2. Le concept des pods miroirs
3. Une démonstration pratique

## Qu'est-ce qu'un pod statique ?

Un pod statique est un pod géré directement par un kubelet sur un nœud et non par le serveur API de Kubernetes. Les pods statiques peuvent fonctionner même s'il n'y a pas de serveur API Kubernetes présent. Si vous disposez simplement d'une machine avec un kubelet en cours d'exécution, vous pouvez créer des pods statiques sur cette machine sans avoir besoin d'un plan de contrôle Kubernetes.

Le kubelet crée automatiquement des pods statiques à partir des fichiers manifestes YAML situés dans le chemin de manifeste sur le nœud. Il y a un chemin de fichier spécifique sur le nœud de travail, et si vous y placez des fichiers de définition de pod YAML, le kubelet créera automatiquement des pods à partir de ces fichiers de définition de pod sans être dirigé par le serveur API de Kubernetes.

## Le concept des pods miroirs

Lorsque le kubelet crée un pod statique, il crée automatiquement un pod miroir pour représenter ce pod statique dans l'API Kubernetes. Ce pod miroir vous permet de voir ce pod via l'API Kubernetes, de voir son statut, mais vous ne pouvez pas apporter de modifications à ce pod ni le gérer via l'API Kubernetes. Il doit être géré directement via le kubelet. Le pod miroir est essentiellement une représentation fantôme du pod statique dans l'API Kubernetes, vous permettant de le visualiser sans pouvoir le modifier.

## Démonstration pratique

Voyons à quoi ressemblent les pods statiques dans notre cluster Kubernetes.

1. **Connexion à un nœud de travail**

   Connectez-vous à l'un de vos nœuds de travail. Pour cette démonstration, cela n'a pas d'importance quel nœud de travail vous choisissez. Connectez-vous simplement à l'un d'entre eux.

```sh
ssh cloud_user@<PUBLIC_IP_ADDRESS>
```

2. **Création d'un fichier de manifeste de pod statique**

   Créez un fichier de manifeste YAML pour le pod statique. Par défaut, le chemin de manifeste configuré par kubeadm est `/etc/kubernetes/manifests`. Créez un fichier nommé `my-static-pod.yaml` dans ce répertoire.

```sh
sudo nano /etc/kubernetes/manifests/my-static-pod.yaml
```

   Ajoutez le contenu suivant au fichier :

   ```yaml
   apiVersion: v1
   kind: Pod
   metadata:
     name: my-static-pod
   spec:
     containers:
     - name: nginx
       image: nginx
   ```

   Enregistrez et quittez le fichier

3. **Redémarrage du kubelet**

   Le kubelet vérifiera automatiquement ce répertoire et créera le pod statique. Si vous ne voulez pas attendre, vous pouvez forcer cette action en redémarrant le kubelet.

```sh
sudo systemctl restart kubelet
```

4. **Vérification du pod statique**

   Passez à votre serveur de contrôle Kubernetes et utilisez l'API Kubernetes pour vérifier le pod statique. Ce que vous verrez est en fait le pod miroir créé par le kubelet.

```sh
kubectl get pods
```

   Vous devriez voir `my-static-pod`. Essayez de supprimer ce pod pour voir ce qui se passe.

```sh
kubectl delete pod my-static-pod-<worker-node-name>
kubectl get pods
```

   Vous verrez que le pod revient immédiatement. Le kubelet recrée automatiquement le pod miroir.

## Conclusion

Pour récapituler, nous avons répondu à la question "Qu'est-ce qu'un pod statique ?", parlé du concept des pods miroirs, et réalisé une démonstration pratique en créant un pod statique dans notre cluster. C'est tout pour cette leçon. À la prochaine !


# Reférences

https://kubernetes.io/docs/tasks/configure-pod-container/static-pod/